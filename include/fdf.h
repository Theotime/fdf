/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/17 06:26:09 by triviere          #+#    #+#             */
/*   Updated: 2016/01/04 16:37:21 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# define FDF_MIN_WIDTH				100
# define FDF_MIN_HEIGHT				100

# define FDF_DEFAULT_WIDTH			600
# define FDF_DEFAULT_HEIGHT			600
# define FDF_DEFAULT_MARGE			1
# define FDF_DEFAULT_INTENSITY		1
# define FDF_DEFAULT_VIEW			1

# define FDF_MIN_COLOR				0x000000
# define FDF_MAX_COLOR				0xFFFFFF

# define FDF_DEFAULT_TITLE			"FDF"

# include "libft.h"
# include "mlx.h"
# include <stdio.h>

typedef struct s_fdf	t_fdf;
typedef struct s_point	t_point;

struct					s_fdf
{
	int					width;
	int					height;
	int					line_len;
	int					nb_line;

	int					min;
	int					max;
	int					view;
	int					marge;
	int					intensity;
	int					wm;
	int					hm;
	int					zm;
	int					color;

	void				*mlx;
	void				*win;

	char				*title;

	t_point				***map;
	t_lst				*points;

	int					(*parse_map)(void);
	int					(*get_color)(int z);

	void				(*start)(void);
	void				(*draw)(int y, int x);
	void				(*draw_color)(int y, int x, int z);
	void				(*draw_points)(void);
	void				(*draw_map)(void);
	void				(*draw_point)(t_point *point);
	void				(*draw_line)(t_point *a, t_point *b);
	void				(*draw_left)(t_fdf *fdf, t_point *a, t_point *b);
	void				(*draw_right)(t_fdf *fdf, t_point *a, t_point *b);
	void				(*draw_up)(t_fdf *fdf, t_point *a, t_point *b);
	void				(*draw_down)(t_fdf *fdf, t_point *a, t_point *b);
	void				(*draw_info)(void);

	void				(*change_view)(void);
	void				(*marge_less)(void);
};

struct					s_point
{
	int					x;
	int					y;
	int					z;
};

/*
** FDF FUNCTION
*/
int						ft_fdf_get_intensity(void);
int						ft_fdf_get_marge(void);
int						ft_fdf_get_view(void);

void					ft_fdf_change_view(void);
void					ft_fdf_marge_less(void);

int						ft_fdf_parse_map(void);
int						ft_fdf_get_color(int z);

void					ft_fdf_init(void);
void					ft_fdf_mlx_init(void);
void					ft_fdf_draw(int y, int x);
void					ft_fdf_draw_color(int y, int x, int z);
void					ft_fdf_draw_points(void);
void					ft_fdf_draw_map(void);
void					ft_fdf_draw_point(t_point *point);
void					ft_fdf_draw_line(t_point *a, t_point *b);
void					ft_fdf_draw_left(t_fdf *fdf, t_point *a, t_point *b);
void					ft_fdf_draw_right(t_fdf *fdf, t_point *a, t_point *b);
void					ft_fdf_draw_up(t_fdf *fdf, t_point *a, t_point *b);
void					ft_fdf_draw_down(t_fdf *fdf, t_point *a, t_point *b);
void					ft_fdf_draw_info(void);
/*
** END FDF FUNCTION
*/

/*
** POINT FUNCTION
*/
t_point					*ft_point_init(int x, int y, int z);

int						ft_fdf_get_x(t_point *p);
int						ft_fdf_get_y(t_point *p);
/*
** END POINT FUNCTION
*/

#endif
