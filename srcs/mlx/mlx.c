/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 14:57:41 by triviere          #+#    #+#             */
/*   Updated: 2016/01/04 16:46:51 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	ft_fdf_display_window(void)
{
	t_fdf		*fdf;

	fdf = ft_get_sys()->get_pgm();
	mlx_clear_window(fdf->mlx, fdf->win);
	fdf->draw_map();
	fdf->draw_info();
}

static int	ft_fdf_mlx_key_hook2(keypress)
{
	t_fdf		*fdf;

	fdf = ft_get_sys()->get_pgm();
	if (keypress == 12 || keypress == 53)
		exit(0);
	else if (keypress == 27)
		fdf->marge_less();
	else if (keypress == 24)
		++fdf->marge;
	else if (keypress == 49)
		fdf->change_view();
	else if (keypress == 43)
		--fdf->intensity;
	else if (keypress == 47)
		++fdf->intensity;
	else if (keypress == 123)
		fdf->wm += 10;
	else if (keypress == 124)
		fdf->wm -= 10;
	else
		return (0);
	return (1);
}

static int	ft_fdf_mlx_key_hook(int keypress, t_fdf *fdf)
{
	if (keypress == 125)
		fdf->hm -= 10;
	else if (keypress == 8)
		fdf->color = !fdf->color;
	else if (keypress == 126)
		fdf->hm += 10;
	else if (keypress == 35)
		++fdf->zm;
	else if (keypress == 46)
		--fdf->zm;
	else if (!ft_fdf_mlx_key_hook2(keypress))
		return (0);
	ft_fdf_display_window();
	return (1);
}

static int	ft_fdf_mlx_expose_hook(t_fdf *fdf)
{
	mlx_clear_window(fdf->mlx, fdf->win);
	fdf->draw_map();
	fdf->draw_info();
	return (0);
}

void		ft_fdf_mlx_init(void)
{
	t_sys		*sys;
	t_fdf		*fdf;

	sys = ft_get_sys();
	fdf = sys->get_pgm();
	mlx_key_hook(fdf->win, ft_fdf_mlx_key_hook, fdf);
	mlx_expose_hook(fdf->win, ft_fdf_mlx_expose_hook, fdf);
	mlx_loop(fdf->mlx);
}
