/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   point.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 12:58:53 by triviere          #+#    #+#             */
/*   Updated: 2015/12/31 14:52:51 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_point		*ft_point_init(int x, int y, int z)
{
	t_point		*point;

	point = ft_memalloc(sizeof(t_point*));
	point->x = x;
	point->y = y;
	point->z = z;
	return (point);
}
