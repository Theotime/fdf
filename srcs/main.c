/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 14:56:20 by triviere          #+#    #+#             */
/*   Updated: 2016/01/04 17:41:59 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		ft_fdf_add_flag(void)
{
	t_sys		*sys;

	sys = ft_get_sys();
	sys->add_flag('w', "width", 1, 1);
	sys->add_flag('h', "height", 1, 1);
	sys->add_flag('t', "title", 1, 1);
	sys->add_flag('m', "marge", 1, 1);
	sys->add_flag('i', "intensity", 1, 1);
	sys->add_flag('v', "view", 1, 1);
}

int				main(int ac, char **av)
{
	t_sys		*sys;
	t_fdf		*fdf;

	fdf = NULL;
	sys = ft_get_sys();
	ft_fdf_add_flag();
	sys->init(ac, av, sizeof(t_fdf));
	sys->check_flags();
	if (sys->av->len != 1)
		sys->die("usage : ./fdf <filename>");
	ft_fdf_init();
	fdf = sys->get_pgm();
	fdf->start();
	return (0);
}
