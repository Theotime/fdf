/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 12:58:48 by triviere          #+#    #+#             */
/*   Updated: 2016/01/04 16:20:48 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		ft_fdf_draw(int y, int x)
{
	t_fdf		*fdf;

	fdf = ft_get_sys()->get_pgm();
	mlx_pixel_put(fdf->mlx, fdf->win, x + fdf->wm, y + fdf->hm, 0xFFFFFF);
}

void		ft_fdf_draw_color(int y, int x, int z)
{
	t_fdf		*fdf;

	fdf = ft_get_sys()->get_pgm();
	if (x < 0 - fdf->wm || y < 0 - fdf->hm \
		|| x > fdf->width - fdf->wm || y > fdf->height - fdf->hm)
		return ;
	mlx_pixel_put(fdf->mlx, fdf->win, x + fdf->wm, y + fdf->hm, \
	fdf->get_color(z + fdf->zm));
}

void		ft_fdf_draw_point(t_point *point)
{
	t_fdf	*fdf;

	fdf = ft_get_sys()->get_pgm();
	fdf->draw_color(point->y, point->x, point->z);
}

void		ft_fdf_draw_points(void)
{
	t_el		*cur;
	t_fdf		*fdf;
	t_point		*point;

	fdf = ft_get_sys()->get_pgm();
	cur = fdf->points->start;
	while (cur)
	{
		point = (t_point*)cur->data;
		fdf->draw_point(point);
		cur = cur->next;
	}
}

void		ft_fdf_draw_map(void)
{
	int			x;
	int			y;
	t_fdf		*fdf;

	fdf = ft_get_sys()->get_pgm();
	y = 0;
	while (y < fdf->nb_line)
	{
		x = 0;
		while (x < fdf->line_len)
		{
			if (x > 0)
				fdf->draw_line(fdf->map[y][x], fdf->map[y][x - 1]);
			if (y > 0)
				fdf->draw_line(fdf->map[y][x], fdf->map[y - 1][x]);
			++x;
		}
		++y;
	}
}
