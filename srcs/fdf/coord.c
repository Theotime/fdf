/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   coord.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 16:20:10 by triviere          #+#    #+#             */
/*   Updated: 2016/01/04 16:22:58 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	ft_fdf_center_width(void)
{
	t_fdf		*fdf;
	int			width;

	fdf = ft_get_sys()->get_pgm();
	if (fdf->view == 1)
		width = fdf->line_len * (fdf->marge / 2);
	else
		width = fdf->line_len * fdf->marge;
	return ((fdf->width - width) / 2);
}

static int	ft_fdf_center_height(void)
{
	t_fdf		*fdf;
	int			height;

	fdf = ft_get_sys()->get_pgm();
	if (fdf->view == 1)
		height = fdf->nb_line * (fdf->marge);
	else
		height = fdf->nb_line * fdf->marge;
	return ((fdf->height - height) / 2);
}

int			ft_fdf_get_x(t_point *p)
{
	t_fdf	*fdf;

	fdf = ft_get_sys()->get_pgm();
	if (fdf->view == 1)
	{
		return ((p->x * fdf->marge) - (p->y * fdf->marge) \
		+ ft_fdf_center_width());
	}
	else
		return (p->x * fdf->marge + ft_fdf_center_width());
}

int			ft_fdf_get_y(t_point *p)
{
	t_fdf	*fdf;

	fdf = ft_get_sys()->get_pgm();
	if (fdf->view == 1)
		return ((p->x * fdf->marge) + p->y * fdf->marge) / 2 + \
		ft_fdf_center_height() + (p->z * (1.0 * fdf->marge / 10) \
		* fdf->intensity * -1);
	else
		return (p->y * fdf->marge + ft_fdf_center_height());
}
