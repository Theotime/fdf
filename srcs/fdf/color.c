/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 15:28:53 by triviere          #+#    #+#             */
/*   Updated: 2016/01/04 16:36:03 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int		ft_fdf_get_color_pos2(int color)
{
	if (color > 15)
		return (0x00611D);
	else if (color > 7)
		return (0x007A25);
	else if (color > 5)
		return (0x00912C);
	else if (color > 2)
		return (0x02A633);
	else if (color > 0)
		return (0x00A632);
	return (0x00D13F);
}

static int		ft_fdf_get_color_pos(int z)
{
	int			color;
	t_fdf		*fdf;

	fdf = ft_get_sys()->get_pgm();
	color = (z * 1.0 / fdf->max) * 100;
	if (color > 85)
		return (0xFFFFFF);
	else if (color > 80)
		return (0xE0C0AF);
	else if (color > 75)
		return (0xD1997B);
	else if (color > 65)
		return (0xBF7B56);
	else if (color > 50)
		return (0xA85F38);
	else if (color > 40)
		return (0x612200);
	else if (color > 25)
		return (0x802D01);
	else if (color > 25)
		return (0x004715);
	return (ft_fdf_get_color_pos2(color));
}

static int		ft_fdf_get_color_neg(int z)
{
	int			color;
	t_fdf		*fdf;

	fdf = ft_get_sys()->get_pgm();
	color = (z * 1.0 / fdf->min) * 100;
	if (color > 80)
		return (0x00204A);
	else if (color > 60)
		return (0x003273);
	else if (color > 40)
		return (0x024CAD);
	else if (color > 25)
		return (0x045DD1);
	else if (color > 14)
		return (0x006BF7);
	else if (color > 6)
		return (0x0567E8);
	else if (color > 2)
		return (0x056EF7);
	else if (color > 0)
		return (0x106BE3);
	return (0x3386F2);
}

int				ft_fdf_get_color(int z)
{
	t_fdf	*fdf;

	fdf = ft_get_sys()->get_pgm();
	if (!fdf->color)
		return (0xFFFFFF);
	else if (z >= 0)
		return (ft_fdf_get_color_pos(z));
	return (ft_fdf_get_color_neg(z));
}
