/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 14:55:58 by triviere          #+#    #+#             */
/*   Updated: 2015/12/30 15:25:08 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	ft_fdf_insert_point_in_map(t_fdf *fdf)
{
	int		x;
	int		y;
	t_el	*cur;

	y = 0;
	cur = fdf->points->start;
	fdf->min = ((t_point*)cur->data)->z;
	fdf->max = ((t_point*)cur->data)->z;
	while (y < fdf->nb_line)
	{
		x = 0;
		fdf->map[y] = ft_memalloc(sizeof(t_point*) * fdf->line_len);
		while (x < fdf->line_len)
		{
			fdf->map[y][x] = ((t_point*)cur->data);
			if (fdf->map[y][x]->z > fdf->max)
				fdf->max = fdf->map[y][x]->z;
			if (fdf->map[y][x]->z < fdf->min)
				fdf->min = fdf->map[y][x]->z;
			cur = cur->next;
			++x;
		}
		++y;
	}
}

static void	ft_fdf_convert_points_to_map(void)
{
	t_fdf	*fdf;

	fdf = ft_get_sys()->get_pgm();
	fdf->nb_line = fdf->points->len / fdf->line_len;
	fdf->map = ft_memalloc(sizeof(t_point*) * fdf->nb_line);
	ft_fdf_insert_point_in_map(fdf);
}

static int	ft_fdf_parse_line(char *line, int y)
{
	char			**columns;
	int				x;
	t_point			*point;
	t_fdf			*fdf;

	x = 0;
	columns = ft_strsplit(line, ' ');
	fdf = ft_get_sys()->get_pgm();
	while (columns[x])
	{
		if (!ft_isnumeric(columns[x]))
			ft_get_sys()->die("file bad content");
		point = ft_point_init(x, y, ft_atoi(columns[x]));
		ft_lst_push(fdf->points, ft_el_init((void*)point));
		++x;
	}
	if (fdf->line_len == -1)
		fdf->line_len = x;
	else if (x != fdf->line_len)
		ft_get_sys()->die("file bad format line is not equals");
	return (1);
}

static int	ft_fdf_parse_file(char *file)
{
	int		fd;
	int		ret;
	char	*line;
	int		y;

	fd = open(file, O_RDONLY);
	y = 0;
	while ((ret = get_next_line(fd, &line)) == 1)
	{
		ft_fdf_parse_line(line, y);
		++y;
	}
	close(fd);
	if (ret != -1)
	{
		ft_fdf_convert_points_to_map();
		return (1);
	}
	return (0);
}

int			ft_fdf_parse_map(void)
{
	t_sys		*sys;

	sys = ft_get_sys();
	if (!ft_fdf_parse_file((char*)sys->av->start->data))
		sys->die("File not found");
	return (0);
}
