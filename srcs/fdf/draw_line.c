/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 14:55:39 by triviere          #+#    #+#             */
/*   Updated: 2016/01/02 12:27:47 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		ft_fdf_draw_right(t_fdf *fdf, t_point *a, t_point *b)
{
	int		x;
	int		xa;
	int		ya;
	int		xb;
	int		yb;

	xa = ft_fdf_get_x(a);
	xb = ft_fdf_get_x(b);
	ya = ft_fdf_get_y(a);
	yb = ft_fdf_get_y(b);
	x = xa;
	while (x < xb)
	{
		fdf->draw_color(ya + ((yb - ya) * (x - xa)) / (xb - xa), x, a->z);
		++x;
	}
}

void		ft_fdf_draw_left(t_fdf *fdf, t_point *a, t_point *b)
{
	int		x;
	int		xa;
	int		ya;
	int		xb;
	int		yb;

	xa = ft_fdf_get_x(a);
	xb = ft_fdf_get_x(b);
	ya = ft_fdf_get_y(a);
	yb = ft_fdf_get_y(b);
	x = xb;
	while (x < xa)
	{
		fdf->draw_color(yb + ((ya - yb) * (x - xb)) / (xa - xb), x, b->z);
		++x;
	}
}

void		ft_fdf_draw_up(t_fdf *fdf, t_point *a, t_point *b)
{
	int		y;
	int		xa;
	int		ya;
	int		xb;
	int		yb;

	xa = ft_fdf_get_x(a);
	xb = ft_fdf_get_x(b);
	ya = ft_fdf_get_y(a);
	yb = ft_fdf_get_y(b);
	y = yb;
	while (y < ya)
	{
		fdf->draw_color(y, xb + ((xa - xb) * (y - yb)) / (ya - yb), b->z);
		++y;
	}
}

void		ft_fdf_draw_down(t_fdf *fdf, t_point *a, t_point *b)
{
	int		y;
	int		xa;
	int		ya;
	int		xb;
	int		yb;

	xa = ft_fdf_get_x(a);
	xb = ft_fdf_get_x(b);
	ya = ft_fdf_get_y(a);
	yb = ft_fdf_get_y(b);
	y = ya;
	while (y < yb)
	{
		fdf->draw_color(y, xa + ((xb - xa) * (y - ya)) / (yb - ya), a->z);
		++y;
	}
}

void		ft_fdf_draw_line(t_point *a, t_point *b)
{
	t_fdf	*fdf;

	fdf = ft_get_sys()->get_pgm();
	if (a->x <= b->x && (b->x - a->x) >= ft_abs(b->y - a->y))
		fdf->draw_right(fdf, a, b);
	else if (b->x <= a->x && (a->x - b->x) >= ft_abs(a->y - b->y))
		fdf->draw_left(fdf, a, b);
	else if (a->y <= b->y && (b->y - a->y) >= ft_abs(b->x - a->x))
		fdf->draw_down(fdf, a, b);
	else if (b->y <= a->y && (a->y - b->y) >= ft_abs(a->x - b->x))
		fdf->draw_up(fdf, a, b);
}
