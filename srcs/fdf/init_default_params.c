/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_default_params.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 16:25:56 by triviere          #+#    #+#             */
/*   Updated: 2016/01/04 16:26:19 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int			ft_fdf_get_intensity(void)
{
	t_sys		*sys;
	t_fdf		*fdf;
	char		*sintensity;
	int			instensity;

	sys = ft_get_sys();
	fdf = sys->get_pgm();
	if (!sys->lflag_active("intensity"))
		return (FDF_DEFAULT_INTENSITY);
	sintensity = (char*)sys->get_params_sflag('i')->start->data;
	if (!ft_isnumeric(sintensity))
		return (FDF_DEFAULT_INTENSITY);
	instensity = ft_atoi(sintensity);
	return (instensity > 0 ? instensity : FDF_DEFAULT_INTENSITY);
}

int			ft_fdf_get_marge(void)
{
	t_sys		*sys;
	t_fdf		*fdf;
	char		*smarge;
	int			marge;

	sys = ft_get_sys();
	fdf = sys->get_pgm();
	if (!sys->lflag_active("marge"))
		return (FDF_DEFAULT_MARGE);
	smarge = (char*)sys->get_params_sflag('m')->start->data;
	if (!ft_isnumeric(smarge))
		return (FDF_DEFAULT_MARGE);
	marge = ft_atoi(smarge);
	return (marge > 0 ? marge : FDF_DEFAULT_MARGE);
}

int			ft_fdf_get_view(void)
{
	t_sys		*sys;
	t_fdf		*fdf;
	char		*sview;
	int			view;

	sys = ft_get_sys();
	fdf = sys->get_pgm();
	if (!sys->lflag_active("view"))
		return (FDF_DEFAULT_VIEW);
	sview = (char*)sys->get_params_sflag('v')->start->data;
	if (!ft_isnumeric(sview))
		return (FDF_DEFAULT_VIEW);
	view = ft_atoi(sview);
	return (view > 0 ? view : FDF_DEFAULT_VIEW);
}
