/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   info.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 16:23:10 by triviere          #+#    #+#             */
/*   Updated: 2016/01/04 16:23:31 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	ft_fdf_draw_string(int x, int y, char *str)
{
	t_fdf		*fdf;

	fdf = ft_get_sys()->get_pgm();
	mlx_string_put(fdf->mlx, fdf->win, x, y, 0xFFFFFF, str);
}

void		ft_fdf_draw_info(void)
{
	t_fdf		*fdf;

	fdf = ft_get_sys()->get_pgm();
	ft_fdf_draw_string(20, 20, ft_strjoin("MARGE : ", ft_itoa(fdf->marge)));
	ft_fdf_draw_string(20, 40, ft_strjoin("INTENSITY : ", \
	ft_itoa(fdf->intensity)));
	ft_fdf_draw_string(20, 60, ft_strjoin("MIN : ", ft_itoa(fdf->min)));
	ft_fdf_draw_string(20, 80, ft_strjoin("MAX : ", ft_itoa(fdf->max)));
}
