/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   event.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 16:21:50 by triviere          #+#    #+#             */
/*   Updated: 2016/01/04 16:21:51 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void			ft_fdf_change_view(void)
{
	t_fdf		*fdf;

	fdf = ft_get_sys()->get_pgm();
	fdf->view = !fdf->view;
}

void			ft_fdf_marge_less(void)
{
	t_fdf		*fdf;

	fdf = ft_get_sys()->get_pgm();
	if (fdf->marge > 1)
		--fdf->marge;
}
