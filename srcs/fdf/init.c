/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 14:56:09 by triviere          #+#    #+#             */
/*   Updated: 2016/01/02 16:49:27 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		ft_fdf_init_fn(void)
{
	t_fdf		*fdf;

	fdf = ft_get_sys()->get_pgm();
	fdf->parse_map = &ft_fdf_parse_map;
	fdf->get_color = &ft_fdf_get_color;
	fdf->start = &ft_fdf_mlx_init;
	fdf->draw = &ft_fdf_draw;
	fdf->draw_color = &ft_fdf_draw_color;
	fdf->draw_points = &ft_fdf_draw_points;
	fdf->draw_point = &ft_fdf_draw_point;
	fdf->draw_map = &ft_fdf_draw_map;
	fdf->draw_line = &ft_fdf_draw_line;
	fdf->draw_right = &ft_fdf_draw_right;
	fdf->draw_left = &ft_fdf_draw_left;
	fdf->draw_up = &ft_fdf_draw_up;
	fdf->draw_down = &ft_fdf_draw_down;
	fdf->change_view = &ft_fdf_change_view;
	fdf->draw_info = &ft_fdf_draw_info;
	fdf->marge_less = ft_fdf_marge_less;
}

static int		ft_fdf_get_width(void)
{
	t_sys		*sys;
	char		*width;
	int			w;

	width = NULL;
	w = 0;
	sys = ft_get_sys();
	if (!sys->lflag_active("width"))
		return (FDF_DEFAULT_WIDTH);
	width = (char*)sys->get_params_sflag('w')->start->data;
	if (!ft_isnumeric(width))
		return (FDF_DEFAULT_WIDTH);
	w = ft_atoi(width);
	if (w > FDF_MIN_WIDTH)
		return (w);
	return (FDF_MIN_WIDTH);
}

static int		ft_fdf_get_heigth(void)
{
	t_sys		*sys;
	char		*height;
	int			h;

	height = NULL;
	h = 0;
	sys = ft_get_sys();
	if (!sys->lflag_active("height"))
		return (FDF_DEFAULT_HEIGHT);
	height = (char*)sys->get_params_sflag('h')->start->data;
	if (!ft_isnumeric(height))
		return (FDF_DEFAULT_HEIGHT);
	h = ft_atoi(height);
	if (h > FDF_MIN_HEIGHT)
		return (h);
	return (FDF_MIN_HEIGHT);
}

static char		*ft_fdf_get_title(void)
{
	t_sys	*sys;

	sys = ft_get_sys();
	if (!sys->lflag_active("title"))
		return (FDF_DEFAULT_TITLE);
	return (char*)sys->get_params_sflag('t')->start->data;
}

void			ft_fdf_init(void)
{
	t_fdf		*fdf;
	t_sys		*sys;

	sys = ft_get_sys();
	fdf = sys->get_pgm();
	fdf->min = 0;
	fdf->max = 0;
	ft_fdf_init_fn();
	fdf->map = NULL;
	fdf->nb_line = -1;
	fdf->line_len = -1;
	fdf->points = ft_lst_init();
	fdf->parse_map();
	fdf->width = ft_fdf_get_width();
	fdf->height = ft_fdf_get_heigth();
	fdf->title = ft_fdf_get_title();
	fdf->view = ft_fdf_get_view();
	fdf->marge = ft_fdf_get_marge();
	fdf->intensity = ft_fdf_get_intensity();
	fdf->mlx = mlx_init();
	fdf->win = mlx_new_window(fdf->mlx, fdf->width, fdf->height, fdf->title);
}
