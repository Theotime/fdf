# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: revers <revers@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/02 03:08:57 by triviere          #+#    #+#              #
#    Updated: 2016/01/04 16:54:35 by triviere         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC=gcc

DIR_SRC=srcs
DIR_OBJ=objs

SRCS=$(shell find $(DIR_SRC) -type f)

INCLUDES=-Iinclude -ILib/libft/includes -ILib/minilibx/ -I/usr/X11/include/

CFLAGS=-Wall -Werror -Wextra -g3 $(INCLUDES)
LDFLAGS=-LLib/libft -lft -LLib/minilibx -lmlx \
	-framework OpenGL -framework AppKit

NAME=fdf

OBJS=$(patsubst $(DIR_SRC)/%,$(DIR_OBJ)/%,$(SRCS:.c=.o))

all: $(NAME)

re : fclean $(NAME)

clean: lib_clean
	rm -rf $(DIR_OBJ)

fclean: lib_fclean clean
	rm -rf ${NAME}

$(NAME): $(OBJS)
	make -C Lib/libft
	make -C Lib/minilibx
	$(CC) -o $@ $^ $(LDFLAGS)

$(DIR_OBJ)/%.o : $(DIR_SRC)/%.c
	mkdir -p $(dir $@)
	$(CC) -o $@ -c $< $(CFLAGS)

lib_clean:
	make clean -C Lib/libft
	make clean -C Lib/minilibx

lib_fclean:
	make fclean -C Lib/libft

run: all

.PHONY: clean fclean re all $(NAME)


