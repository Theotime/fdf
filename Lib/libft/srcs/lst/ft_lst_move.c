/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_move.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:10:56 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:14:50 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_lst_swipe_back_to_front(t_lst *lst)
{
	t_el		*tmp;

	tmp = ft_lst_pop(lst);
	if (tmp)
		ft_lst_unshift(lst, tmp);
}

void		ft_lst_swipe_front_to_back(t_lst *lst)
{
	t_el		*tmp;

	tmp = ft_lst_slice(lst);
	if (tmp)
		ft_lst_push(lst, tmp);
}

void		ft_lst_swipe_check_start_end(t_lst *lst, t_el *a, t_el *b)
{
	if (a->prev)
		a->prev->next = a;
	else
		lst->start = a;
	if (a->next)
		a->next->prev = a;
	else
		lst->end = a;
	if (b->prev)
		b->prev->next = b;
	else
		lst->start = b;
	if (b->next)
		b->next->prev = b;
	else
		lst->end = b;
}

void		ft_lst_swipe(t_lst *lst, t_el *a, t_el *b)
{
	t_el	*tmp;

	if (!a || !b)
		return ;
	tmp = a->next;
	a->next = ft_memcmp(a, b->next, sizeof(t_el)) ? b->next : b;
	b->next = ft_memcmp(b, tmp, sizeof(t_el)) ? tmp : a;
	tmp = a->prev;
	a->prev = ft_memcmp(a, b->prev, sizeof(t_el)) ? b->prev : b;
	b->prev = ft_memcmp(b, tmp, sizeof(t_el)) ? tmp : a;
	ft_lst_swipe_check_start_end(lst, a, b);
}

t_el		*ft_lst_remove(t_lst *lst, t_el *el)
{
	t_el		*tmp;

	if (!el)
		return (NULL);
	tmp = el->prev;
	if (tmp)
		tmp->next = el->next;
	else
		lst->start = el->next;
	tmp = el->next;
	if (tmp)
		tmp->prev = el->prev;
	else
		lst->end = el->prev;
	el->prev = NULL;
	el->next = NULL;
	--lst->len;
	return (el);
}
