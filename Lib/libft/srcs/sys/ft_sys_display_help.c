/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sys_display_help.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:10:20 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:10:21 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_sys_display_help(void)
{
	t_sys		*sys;
	t_el		*cur;

	sys = ft_get_sys();
	cur = sys->flags->start;
	ft_putendl("OPTIONS :");
	while (cur)
	{
		ft_putstr("	-");
		ft_putchar(((t_flag*)cur->data)->sname);
		ft_putstr(" --");
		ft_putstr(((t_flag*)cur->data)->lname);
		ft_putchar('\n');
		cur = cur->next;
	}
	exit(0);
}
