/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_el.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:19:07 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:19:19 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_el		*ft_el_init(void *data)
{
	t_el	*el;

	el = malloc(sizeof(t_el));
	ft_bzero((void*)el, sizeof(t_el));
	el->data = data;
	return (el);
}
