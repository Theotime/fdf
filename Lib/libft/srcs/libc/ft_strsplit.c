/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 23:15:51 by triviere          #+#    #+#             */
/*   Updated: 2015/12/28 16:17:35 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

static int		ft_nbr_word(char const *s, char c)
{
	int		i;
	int		nbr;

	i = 0;
	nbr = 0;
	while (s[i] != '\0')
	{
		while (s[i] == c)
			i++;
		if (s[i] != c && s[i] != '\0')
		{
			i++;
			nbr++;
		}
		while (s[i] != c && s[i] != '\0')
			i++;
	}
	return (nbr);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**result;
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 0;
	result = (char**)ft_memalloc(sizeof(char*) * (ft_nbr_word(s, c) + 1));
	while (s[i] != '\0')
	{
		k = 0;
		while (s[i] == c)
			i++;
		while (s[i] != c && s[i] != '\0' && (++k))
			i++;
		if (k > 0)
		{
			result[j++] = (char*)ft_memalloc(sizeof(char) * k + 1);
			ft_strncpy(result[j - 1], s + (i - k), k);
		}
	}
	return (result);
}
