/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sys.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:07:09 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:08:25 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SYS_H
# define FT_SYS_H

# include <stddef.h>
# include "libft.h"

typedef struct s_flag	t_flag;
typedef struct s_sys	t_sys;

struct					s_flag
{
	char				sname;
	char				*lname;
	int					min;
	int					max;
	int					active;
	t_lst				*args;
};

struct					s_sys
{
	void				*pgm;
	char				*name;
	t_lst				*flags;
	t_lst				*av;

	void				(*init)(int ac, char **av, size_t pgm_size);
	void				(*add_flag)(char sname, char *lname, int min, int max);
	void				(*check_flags)(void);
	void				(*active_sflag)(char flag);
	void				(*active_lflag)(char *flag);

	void				(*display_help)(void);

	void				(*debug)(char *str);
	void				(*log)(char *str);
	void				(*error)(char *str);
	void				(*bad_flag)(char *str);
	void				(*die)(char *str);

	int					(*sflag_active)(char flag);
	int					(*lflag_active)(char *flag);
	int					(*has_sflag)(char flag);
	int					(*has_lflag)(char *flag);

	t_el				*(*get_sflag)(char flag);
	t_el				*(*get_lflag)(char *flag);

	t_lst				*(*get_params_lflag)(char *flag);
	t_lst				*(*get_params_sflag)(char flag);

	void				*(*get_pgm)(void);
};

t_sys					*ft_get_sys(void);
void					ft_sys_init(int ac, char **av, size_t pgm_size);
void					ft_sys_init_flags(t_sys *sys);
void					ft_sys_add_flag(char sname, char *lname, int min, \
		int max);
void					ft_sys_init_fn(t_sys *sys);
void					ft_sys_check_flags(void);
void					ft_sys_flag_active_lflag(char *flag);
void					ft_sys_flag_active_sflag(char flag);

void					*ft_sys_get_pgm(void);

void					ft_sys_display_help(void);

void					ft_sys_debug(char *str);
void					ft_sys_log(char *str);
void					ft_sys_error(char *str);
void					ft_sys_die(char *str);
void					ft_sys_bag_flag(char *flag);

int						ft_sys_flag_sflag_active(char flag);
int						ft_sys_flag_lflag_active(char *flag);
int						ft_sys_flag_has_sflag(char flag);
int						ft_sys_flag_has_lflag(char *flag);
int						ft_sys_flag_is_flag_format(t_el *flag);
int						ft_sys_flag_parse_sflag_params(t_el *av, char *arg);
int						ft_sys_flag_parse_lflag_params(t_el *av);

t_el					*ft_sys_flag_get_lflag(char *flag);
t_el					*ft_sys_flag_get_sflag(char flag);

t_lst					*ft_sys_flag_get_params_lflag(char *flag);
t_lst					*ft_sys_flag_get_params_sflag(char flag);

#endif
