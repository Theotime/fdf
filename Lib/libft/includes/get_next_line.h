/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/30 14:26:11 by triviere          #+#    #+#             */
/*   Updated: 2015/12/30 14:26:28 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# include "libft.h"
# include <fcntl.h>

# define BUFF_SIZE 80

typedef struct		s_fd
{
	int				fd;
	int				ret;
	int				i;
	int				line;
	char			*old;
	char			*buff;
	struct s_fd		*next;
}					t_fd;

int					get_next_line(int const fd, char **line);

#endif
